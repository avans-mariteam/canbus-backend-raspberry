import math

N_Scale =           0.833333333333
I_Scale =           0.024360000000
T_Scale =           0.100000000000
U_Scale =           0.002929687500
Ubat_Scale =        0.007342529297
Vin_Scale =         0.000201416016
Vout_Scale =        0.000402832031
Uin_Scale =         0.15049
Uout_Scale =        0.209  
Iin_Scale =         0.00872
BMS_Vol_Scale =     0.0001

# Get value from bytearray little endian, startbyte, endbyte, multiplier
def can_c(msg, sb, eb, m):
    if eb - sb == 0:
        return msg.data[sb] * m
    elif eb - sb == 1:
        return (msg.data[sb] | msg.data[eb] << 8) * m
    elif eb - sb == 2:
        return (msg.data[sb] | msg.data[eb - 1] << 8 | msg.data[eb] << 16) * m
    elif eb - sb == 3:
        return (msg.data[sb] | msg.data[eb - 2] << 8 | msg.data[eb -1] << 16  | msg.data[eb] << 24) * m
    return -1

# Get value from bytearray big endian, startbyte, endbyte, multiplier
def can_c_be(msg, sb, eb, m):
    if eb - sb == 0:
        return msg.data[sb] * m
    elif eb - sb == 1:
        return (msg.data[sb] << 8 | msg.data[eb]) * m
    elif eb - sb == 2:
        return (msg.data[sb] << 16 | msg.data[eb - 1] << 8 | msg.data[eb]) * m
    elif eb - sb == 3:
        return (msg.data[sb] << 24 | msg.data[eb - 2] << 16 | msg.data[eb -1] << 8  | msg.data[eb]) * m
    return -1

# Get flag from bytearray, flag bit
def can_b(msg, sbit):
    start_byte = math.floor(sbit / 8)
    rest_bit = sbit % 8
    byte = msg.data[start_byte]
    mask = 1 << rest_bit
    if byte & mask:
        return True
    return False 

def msg_verbose1(d):
    return {"mtr-power": {
        "timestamp": d.timestamp,
        "mbv": can_c(d, 4, 5, Ubat_Scale),
        "pvu": can_c(d, 0, 1, U_Scale),
        "pvv": can_c(d, 2, 3, U_Scale),
        "pvw": can_c(d, 6, 7, U_Scale)
    }}

def msg_verbose2(d):
    return {"mtr-flags": {
        "timestamp": d.timestamp,
        "fs": can_b(d, 0),
        "to": can_b(d, 1),
        "ov": can_b(d, 2),
        "uv": can_b(d, 3),
    }}

def msg_verbose3(d):
    return {"mtr-verbose": {
        "timestamp": d.timestamp,
        "maq": can_c(d, 0, 1, U_Scale),
        "ext": can_c(d, 2, 3, 0.016666667),
        "pcb-t": can_c(d, 4, 5, 0.1),
        "mtr-t": can_c(d, 6, 7, 0.1)    
    }}

def msg_speedhandle(d):
    return {"throttle": {
        "timestamp": d.timestamp,
        "spd": can_c(d, 0, 1, N_Scale),
        "cur": can_c(d, 2, 3, I_Scale) 
    }}

def msg_mtrstate(d):
    return {"mtr-state": {
        "timestamp": d.timestamp,
        "state": can_c(d, 0, 1, 1),
        "enable": can_b(d, 0),
        "ready": can_b(d, 35),
        "status": can_b(d, 33),
        "error": can_b(d, 41)
    }}
    
def msg_phasecurrent(d):
    return {"phase": {
        "timestamp": d.timestamp,
        "cur": can_c(d, 5, 6, I_Scale)
    }}

def msg_voltagespeed(d):
    return {"mtr-speed": {
        "timestamp": d.timestamp,
        "ramp": can_c(d, 0, 1, N_Scale),
        "rpm": can_c(d, 2, 3, N_Scale)
    }}

def msg_turningdir(d):
    return {"steer": {
        "timestamp": d.timestamp,
        "dir": can_b(d, 35)
    }}

def msg_mppt1(d):
    return {"mppt-1": {
        "timestamp": d.timestamp,
        "in-v": can_c(d, 0, 1, Uin_Scale),
        "cur": can_c(d, 2, 3, Iin_Scale),
        "bvlr": can_b(d, 7),
        "ovt": can_b(d, 6),
        "noc": can_b(d, 5),
        "undv": can_b(d, 4)
        #"temp": d.data[7]
    }}

def msg_mppt2(d):
    return {"mppt-2": {
        "timestamp": d.timestamp,
        "in-v": can_c(d, 0, 1, Uin_Scale),
        "cur": can_c(d, 2, 3, Iin_Scale),
        "bvlr": can_b(d, 7),
        "ovt": can_b(d, 6),
        "noc": can_b(d, 5),
        "undv": can_b(d, 4)
    }}

def msg_mppt3(d):
    return {"mppt-3": {
        "timestamp": d.timestamp,
        "in-v": can_c(d, 0, 1, Uin_Scale),
        "cur": can_c(d, 2, 3, Iin_Scale),
        "bvlr": can_b(d, 7),
        "ovt": can_b(d, 6),
        "noc": can_b(d, 5),
        "undv": can_b(d, 4)     
    }}

def msg_mppt4(d):
    return {"mppt-4": {
        "timestamp": d.timestamp,
        "in-v": can_c(d, 0, 1, Uin_Scale),
        "cur": can_c(d, 2, 3, Iin_Scale),
        "bvlr": can_b(d, 7),
        "ovt": can_b(d, 6),
        "noc": can_b(d, 5),
        "undv": can_b(d, 4)
    }}

def msg_bmscellvoltage(d):      #niet veranderd naar json waarde
    key = "bms-" + str(d.data[0])
    return {key: {
        "timestamp": d.timestamp,
        "vol": can_c_be(d,1, 2, 0.0001)
    }}

def msg_bmsmsg1(d):
    return {"bms-msg-1": {
        "timestamp": d.timestamp,
        "high": can_c_be(d, 3, 4, BMS_Vol_Scale),
        "low": can_c_be(d, 5, 6, BMS_Vol_Scale),
        "temp": d.data[2],
        "dre": can_b(d, 0),
        "cre": can_b(d, 1),
        "charging": can_b(d, 7)
    }}

def msg_bmsmsg2(d):
    return {"bms-msg-2": {
        "timestamp": d.timestamp,
        "soc": d.data[0],
        "avg-t": d.data[1],
        "avg-v": can_c_be(d, 2, 3, BMS_Vol_Scale),
        "psv": can_c_be(d, 4, 5, 0.01),
        "aph": can_c_be(d, 6, 7, 0.01)
    }}

def msg_bmsmsg3(d):
    return {"bms-msg-3": {
        "timestamp": d.timestamp,
        "cur": can_c_be(d, 0, 1, 0.1),
        "av-open": can_c_be(d, 2, 3, BMS_Vol_Scale)
    }}

def msg_gpsloc(d):
    return {"gps-loc": {
        "timestamp": d.timestamp,
        "lat": can_c(d, 0, 3, 1) * pow(10, -6),
        "long": can_c(d, 4, 7, 1) * pow(10, -6)
    }}

def msg_gpsinfo(d):
    return {"gps-info": {
        "timestamp": d.timestamp,
        "sats": d.data[0],
        "hdop": can_c(d, 1, 2, 0.01),
        "fix": can_c(d, 3, 3, 1),
        "crs": can_c(d, 4, 5, 0.01),
        "spd": can_c(d, 6, 7, 0.01)
    }}

ids = {
    54: msg_bmscellvoltage,
    112: msg_bmsmsg1,
    128: msg_bmsmsg2,
    144: msg_bmsmsg3,
    256: msg_speedhandle,
    336: msg_gpsloc,
    352: msg_gpsinfo,
    512: msg_mtrstate,
    1040: msg_phasecurrent,
    1296: msg_voltagespeed,
    1568: msg_turningdir,
    1840: msg_verbose1,
    1856: msg_verbose2,
    1872: msg_verbose3,
    1905: msg_mppt1,
    1906: msg_mppt2,
    1907: msg_mppt3,
    1908: msg_mppt4
}

# execute parse function for message
def rx_execute(msg):
    if msg.arbitration_id in ids:
        func = ids.get(msg.arbitration_id)
        return func(msg)
    else:
        print('\033[91m' + "[WARNING] Unknown message: ", msg, '\033[0m')
        return False