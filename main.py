# Avans Solar Mariteam Backend
# This program will extract CANbus data and procces it
# Author: Tim Franken
# Email: timfrankentwf@gmail.com


# System resources
import os
import time
import sys
import threading

# Local resources
from canModule import can_start
from redisModule import redisTest

# init all threads
def init():
    t1 = threading.Thread(target = can)
    t1.start()

# start CAN listening
def can():
    can_start()

# Main program
try:
    if(redisTest):
        print('Redis server is connected!')
        # Bring up can0 interface at 500kbps
        os.system("sudo /sbin/ip link set can0 up type can bitrate 500000")
        time.sleep(0.1)	
        print('Press CTL-C to exit')
        init()

except (KeyboardInterrupt, SystemExit):
	#Catch keyboard interrupt
	os.system("sudo /sbin/ip link set can0 down")
	print('\n\rProgram has been exited... Bye!')