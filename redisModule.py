import time
import redis
import asyncio
import aioredis

__timestamp__ = int((time.time() * 1000))

# Redis test
def redisTest():
    print('timestamp (UTC): ', __timestamp__) 
    try:
        r = redis.Redis(host='localhost', port=6379, db=0)
        r.ping()
        print('Connected to Redis!')
        return True
    except Exception as ex:
        print('Error:', ex)
        exit('Failed to connect, terminating.')
        return False

async def main():
    redis = await aioredis.create_redis_pool('redis://localhost')
    await redis.set('my-key', 'value')
    value = await redis.get('my-key', encoding='utf-8')
    print(value)

    redis.close()
    await redis.wait_closed()

asyncio.run(main())