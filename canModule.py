# Avans Solar Mariteam Backend
# This program will extract CANbus data and procces it
# Author: Tim Franken
# Email: timfrankentwf@gmail.com


# System resources
import can

# Local resources
from message import rx_execute

bus = can.Bus(interface='socketcan', channel='can0', receive_own_messages=True)

# start listening on CAN bus
def can_start():
    print('Start listening CANBUS')
    for msg in bus:
        data = rx_execute(msg)
        if data:
            print(data)